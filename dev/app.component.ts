import {Component} from 'angular2/core';
import {myfirstComponent} from './my-first-component.component';

@Component({
    selector: 'my-app',
    template: ` 
       <h1>TEST</h1>
       <my-component></my-component>
    `,
    directives[myfirstComponent],
})
export class AppComponent {

}