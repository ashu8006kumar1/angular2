import {Component} from 'angular2/core';
@Component({
   selector:'my-component',
   template:`
My name is <span [class.is-cool]="inputValue.value==='yes'" >{{name}}<span>
<input type="text" #inputValue (keyup)="0">
`,
styleUrls:['src/css/mycomponents.css']
})

export class myfirstComponent{
    name="Ashish";
}